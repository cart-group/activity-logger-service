FROM eclipse-temurin:17-jre-alpine
VOLUME /tmp
RUN mkdir /opt/app
COPY target/*.jar /opt/app/activitylog.jar
EXPOSE 9996
CMD [ "java", "-jar", "/opt/app/activitylog.jar"]