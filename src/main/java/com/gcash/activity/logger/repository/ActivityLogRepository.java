package com.gcash.activity.logger.repository;

import java.util.UUID;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.gcash.activity.logger.entity.ActivityLog;

public interface ActivityLogRepository extends MongoRepository<ActivityLog, UUID> {

}
