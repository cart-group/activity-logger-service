package com.gcash.activity.logger.service;

import com.gcash.activity.logger.dto.RequestActivityLog;
import com.gcash.activity.logger.entity.ActivityLog;

public interface ActivityLogService {
	ActivityLog create(RequestActivityLog request);
}
