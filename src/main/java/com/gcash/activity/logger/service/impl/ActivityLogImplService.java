package com.gcash.activity.logger.service.impl;

import org.apache.kafka.common.Uuid;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gcash.activity.logger.dto.RequestActivityLog;
import com.gcash.activity.logger.entity.ActivityLog;
import com.gcash.activity.logger.repository.ActivityLogRepository;
import com.gcash.activity.logger.service.ActivityLogService;

@Service
public class ActivityLogImplService implements ActivityLogService {
	
	@Autowired
	private ActivityLogRepository activityLogRepository;

	@Override
	public ActivityLog create(RequestActivityLog request) {
		ActivityLog activityLog = new ActivityLog();
		BeanUtils.copyProperties(request, activityLog);
		activityLog.setId(Uuid.randomUuid().toString());
		return activityLogRepository.save(activityLog);
	}

}
