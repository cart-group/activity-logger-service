package com.gcash.activity.logger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ActivityLoggerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ActivityLoggerServiceApplication.class, args);
	}

}
