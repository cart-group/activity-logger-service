package com.gcash.activity.logger.entity;

import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document
@Data
public class ActivityLog {
	@Id
	private String id;
	
	private String method;
	
	private String url;
	
	private String transactionReferenceNumber;
	
	private LocalDateTime loggedTime;
}
