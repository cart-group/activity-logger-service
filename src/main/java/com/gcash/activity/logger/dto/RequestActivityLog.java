package com.gcash.activity.logger.dto;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestActivityLog {
	private String method;
	
	private String url;
	
	private String transactionReferenceNumber;
	
	private LocalDateTime loggedTime;

	@Override
	public String toString() {
		return "RequestActivityLog [method=" + method + ", url=" + url + ", transactionReferenceNumber="
				+ transactionReferenceNumber + ", loggedTime=" + loggedTime + "]";
	}
	
	
}
