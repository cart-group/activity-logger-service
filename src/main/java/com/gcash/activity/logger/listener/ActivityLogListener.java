package com.gcash.activity.logger.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.gcash.activity.logger.dto.RequestActivityLog;
import com.gcash.activity.logger.service.ActivityLogService;

@Component
public class ActivityLogListener {
	
	@Autowired
	private ActivityLogService activityLogService;

	@KafkaListener(topics="product-activity", groupId = "product-activity-consumer-group", containerFactory = "kafkaListenerContainerFactory")
	public void listenProductActivity(RequestActivityLog requestActivityLog) {
		activityLogService.create(requestActivityLog);
	}
}
