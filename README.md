# cart-gateway-service



## Docker Image
Docker Repository:

```
https://hub.docker.com/r/villetorio/activity-logger-service
```

Docker pull

```
docker pull villetorio/activity-logger-service
```
## Run via docker compose
```
version: '3.1'
services:
  activity-logger:
    build: .
    ports:
      - "9996:9996"
    environment:
      spring.kafka.bootstrap-servers: "kafka:9092"
      spring.data.mongodb.host: "mongo"
  mongo:
    image: mongo
    restart: always
    environment:
      MONGO_INITDB_ROOT_USERNAME: mongoadmin
      MONGO_INITDB_ROOT_PASSWORD: pass
      MONGO_INITDB_DATABSE: activitylog
    volumes:
      - ./mongo-init.js:/docker-entrypoint-initdb.d/mongo-init.js:ro
      
  mongo-express:
    image: mongo-express
    restart: always
    ports:
      - 8081:8081
    environment:
      ME_CONFIG_MONGODB_ADMINUSERNAME: mongoadmin
      ME_CONFIG_MONGODB_ADMINPASSWORD: pass
      ME_CONFIG_MONGODB_URL: mongodb://mongoadmin:pass@mongo:27017/

networks: 
  default: 
    external: 
      name: external-bridge 
```

## K8s Deployment/Service
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: activity-logger
spec:
  selector:
    matchLabels:
      app: activity-logger
  template:
    metadata:
      labels:
        app: activity-logger
    spec:
      containers:
      - name: activity-logger
        image: villetorio/activity-logger-service:1.0.0
        livenessProbe:
          httpGet:
            path: "/actuator/health/liveness"
            port: 9996
          initialDelaySeconds: 30
          periodSeconds: 10
        readinessProbe:
          httpGet:
            path: "/actuator/health/readiness"
            port: 9996
          initialDelaySeconds: 30
          periodSeconds: 10
        resources:
          limits:
            memory: "1024Mi"
            cpu: "500m"
        ports:
        - containerPort: 9996
        env:
          - name: spring.kafka.bootstrap-servers
            value: kafka-svc:9092
          - name: spring.data.mongodb.host
            value: mongo
---
apiVersion: v1
kind: Service
metadata:
  name: activity-logger-service
spec:
  selector:
    app: activity-logger
  ports:
  - port: 9996
    targetPort: 9996
```
